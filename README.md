# WhatsApp Automate Messages Sender

Little Python automate which sends scheduled messages via WhatsApp Web
Made with only two lines 😁😁

## How to use it ?

#### 1 - Clone the project repository from the master branch to your Computer  

#### 2 - Open the projet in your IDE or editor

#### 3 - Make sure you have installed the pywhatkit library !
If not, get it with this command in your terminal : pip install pywhatkit

#### 4 - The Code
For your business, you will have modify the only function called "pywhatki.sendmsg()" with parameters as below

###### # The Syntaxe
    pywhatkit.sendwhatmsg("number", "Message", hours, minutes)

###### Where
###### 1 - _Number_ : The receiver WhatsApp number
###### 2 - _Message_ : The input message that you want to send
###### 3 - _Hours_ : Time (hour) of message delivery (Should be in 24 hours format)
###### 4 - _Minutes_* : Time (Minutes) of message delivery
###### *For the minutes, I would advise you to choose at least 2 minutes from the current minute to be sure that the process runs completely.

Anothers optional parameters can be added

    pywhatkit.sendwhatmsg("number", "Message", hours, minutes, "wait_time : int = 20",'print-waitTime : bool = True')

###### 5 - _Wait_time_ : Time (seconds) to wait before send the message after the WhatsApp Web is lunched, the default value is 20 
###### 6 - _Print_waitTime_ : Bool variable (True/False) which allows to say if the wait_time function will be considered or not, the default value is True

### #NB : You could also send file and the syntax will be
    pywhatkit.send_file(“number”,”Path to file”,hours,minutes)
####### Example
***
![Cooking my function](https://gitlab.com/sighaloic/whatsapp-automate-messages-sender/-/raw/sighaloic/screenshots/1.jpg)
![Running my little program](https://gitlab.com/sighaloic/whatsapp-automate-messages-sender/-/raw/sighaloic/screenshots/2.png)
![The result](https://gitlab.com/sighaloic/whatsapp-automate-messages-sender/-/raw/sighaloic/screenshots/3.jpg)

***
We could have the log of all our sent messages in the file "pywhatkit_dbs.txt" created after the first message was sent.
***
![Logs](https://gitlab.com/sighaloic/whatsapp-automate-messages-sender/-/raw/sighaloic/screenshots/4.jpg)

***

### Others tools of the same library

#### 1 - Play a video on YouTube

###### # Syntax

    pywhatkit.playonyt("url/topic")

###### _url/topic_ : the url or the topic of the video you want to play on YouTube

#### 2 - Automated Google search

###### # Syntax

    pywhatkit.search("keyword")
###### _keyword_ : what you would search on Google

#### 3 - Printed Text to Handwritten text (.png format)

####### Syntax

    pywhatkit.text_to_handwritten(text, rgb=[0,0,0])
###### _text_ : the text you would parse to handwritten
***
![My text to handwritten](https://gitlab.com/sighaloic/whatsapp-automate-messages-sender/-/raw/sighaloic/pywhatkit.png)
***
#### 4 - Convert an image to ASCII Art 

###### # Syntax

    pywhatkit.image_to_ascii_art(path_to_image, ouput_file)

###### _path_to_image_ : the path where the image is stored in your PC
###### _output_file_ : the output converted file

###### # Example

    pywhatkit.image_to_ascii_art('katua logo.png','katua logo.text')

***
![My ASCII Art](https://gitlab.com/sighaloic/whatsapp-automate-messages-sender/-/raw/sighaloic/screenshots/5.png)
***
It's not pretty well but not bad too 😁

#### 5 - Shut Down your PC 

####### Syntax

    pywhatkit.shutdown(time=100)

###### _time=100_ : The delays before shutting down your PC
###### You can cancel the your shut down request

    pywhatkit.concelShutdown()

***

## ~_Thank you and let's keep in touch 😉🙏🏿_~










